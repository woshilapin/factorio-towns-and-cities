[![build](https://gitlab.com/pizzatorio/factorio-towns-and-cities/badges/master/build.svg)](https://gitlab.com/pizzatorio/factorio-towns-and-cities/pipelines)
[![coverage](https://codecov.io/gl/pizzatorio/factorio-towns-and-cities/branch/master/graph/badge.svg)](https://codecov.io/gl/pizzatorio/factorio-towns-and-cities)

# Town And Cities [Factorio Mod]
This mod will grow you cities requesting for resources.

## Install
Working with Luarocks is going to simplify your life.  On Mac OS X, you can
install it with Homebrew.

```
brew install luarocks
```

Then you can install the dependencies needed.

```
luarocks install busted cluacov luacheck
```

## Tests
To run the test suite, just launch the following command.

```
busted
```

## Quality
Luacheck is the tool we use for checking the quality of the project. To get an
analysis, run the following command.

```
luacheck .
```
