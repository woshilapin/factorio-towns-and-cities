--data.lua


require("prototypes.item.city")
require("prototypes.item.underground")
require("prototypes.item.coin")
require("prototypes.entity.city")
require("prototypes.entity.underground")
require("prototypes.recipe.city")
require("prototypes.recipe.underground")
require("prototypes.recipe.consumable")
require("prototypes.categories.recipe-category")