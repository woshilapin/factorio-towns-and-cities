data:extend({
    {
        type = "recipe",
        name = "city-logistic-chest-requester",
        ingredients = {
            {
                "logistic-chest-requester", 1
            },
            { "steel-plate", 1 },
        },
        result = "city-logistic-chest-requester",
    },
})
