data:extend({
    {
        type = "recipe",
        name = "consumable",
        category = "city-consumer",
        ingredients = {
            { "iron-plate", 5 },
        },
        result = "steel-plate",
    }
})
