data:extend({
    {
        type = "recipe",
        name = "underground-city",
        ingredients = {
            {
                "assembling-machine-1", 1
            },
            { "steel-plate", 1 },
        },
        result = "underground-city",
    }
})
