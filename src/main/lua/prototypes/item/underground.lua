data:extend({
    {
        type = "item",
        name = "underground-city",
        icons = {
            {
                icon = "__base__/graphics/icons/assembling-machine-3.png",
                tint = { r = 1, g = 0, b = 0, a = 0.3 }
            },
        },
        icon_size = 32,
        subgroup = "production-machine",
        order = "c[assembling-machine-3]",
        place_result = "underground-city",
        stack_size = 50
    }
})

