data:extend({
    {
        type = "item",
        name = "consumerism-coin",
        icon = "__base__/graphics/icons/coin.png",
        icon_size = 32,
        flags = {"hidden"},
        subgroup = "science-pack",
        order = "y",
        stack_size = 100000
    },
})

