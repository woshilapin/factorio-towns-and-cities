data:extend({
    {
        type = "int-setting",
        name = "towns-and-cities-underground-spawn-frequency",
        setting_type = "startup",
        minimum_value = 1,
        default_value = 80,
        maximum_value = 100
    }
})