--control.lua

script.on_event({ defines.events.on_tick },
    function(e)
        if not global.money then global.money = 0 end
        if e.tick % 60 == 0 then
            for _, player in pairs(game.players) do
                -- NOTE: to change the caption for money do something like this: player.gui.left.display.main.money_table.money_amount.caption = "foobar"
                -- NOTE: this is just setup to add the display to each player in the game. Updating the counts should happen elsewhere :)
                if not player.gui.left["display"] then
                    -- main frame and setting the veritcallity of it
                    player.gui.left.add { type = "frame", name = "display", direction = "vertical" }

                end
                if player.gui.left.display.main then
                    player.gui.left.display.main.destroy()
                end
                player.gui.left.display.add { type = "table", name = "main", ignored_by_interaction = true, column_count = 1, draw_horizontal_lines = true, enabled = true }

                -- Money table and display text setup
                player.gui.left.display.main.add { type = "table", name = "money_table", ignored_by_interaction = true, column_count = 2, enabled = true }
                player.gui.left.display.main.money_table.add { type = "label", name = "money_label", caption = "money: " }
                player.gui.left.display.main.money_table.add { type = "label", name = "money_amount", caption = "$" .. global.money }

            end
        end
        if e.tick % 60 == 0 then --common trick to reduce how often this runs, we don't want it running every tick, just 1/second
            for _, surface in pairs(game.surfaces) do
                for _, clcr in pairs(surface.find_entities_filtered({ name = "underground-city", limit = 100 })) do
                    local count = clcr.remove_item("consumerism-coin")
                    local recipe = clcr.get_recipe()
                    if recipe then
                        for _, product in pairs(recipe.products) do
                            clcr.remove_item(product.name)
                        end
                    end
                    if count > 0 then
                        log("Clearing city for profit: " .. count)
                        global.money = global.money + count
                    end
                end
            end
        end
    end)

